===============================
JIRA to Datadog Event Publisher
===============================

A microservice that receives JIRA webhook payloads and publishes them to Datadog


The following is a quick introduction to installing and running the service. Please
generate the full documentation for more in-depth instructions (see `Documentation` below)

Installation
------------

Start by creating a virtual environment::

    mkvirtualenv jiradog

Update pip and install the required dependencies::

    pip install --upgrade pip
    pip install .

Usage
-----

In development mode::

    python manage.py <command>

Where command can be any of the following::

    shell               Runs a Python shell inside Flask application context.
    urls                Displays all of the url matching routes for the project
    db                  Perform database migrations
    server              Runs the Flask development server i.e. app.run()
    clean               Remove *.pyc and *.pyo files recursively starting at current directory
    test                Run the tests.
    runserver           Runs the Flask development server i.e. app.run()

Documentation
-------------

To generate the documentation, you will need `tox` installed::

    pip install tox

Generate the documentation by running::

    tox -e docs

Tox will install any required dependencies. This will generate HTML documentation 
under `build/html` in the project root, which can be opened in your browser of choice.


Contributing
------------

Contribute by forking and sending a pull request against this repository.
