=======
Testing
=======

To run the provided unit tests, install tox and run the relevant testenv::

    pip install tox
    tox -e test

This will run all of the unit tests under the `tests/` directory and provide an `xunit.xml` file
as well as HTML coverage statistics under `cover/`.
