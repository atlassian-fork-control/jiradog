========
Usage
========

Running Locally
---------------

For development-based environments, you can make use of the `manage.py` entrypoint into the application. This entrypoint provides a number of useful features that will come in handy during development and testing.

To interact with the server locally::

    python manage.py -h
    usage: manage.py [-?] {shell,urls,db,server,clean,test,runserver} ...

    positional arguments:
      {shell,urls,db,server,clean,test,runserver}
      shell               Runs a Python shell inside Flask application context.
      urls                Displays all of the url matching routes for the project
      db                  Perform database migrations
      server              Runs the Flask development server i.e. app.run()
      clean               Remove *.pyc and *.pyo files recursively starting at current directory
      test                Run the tests.
      runserver           Runs the Flask development server i.e. app.run()

The `shell` argument allows you to inspect variables in the Flask application context::

    python manage.py shell
    >>> app
    <Flask 'flask_aui.app'>
    >> db
    <SQLAlchemy engine='sqlite:////Users/apetrovic/src/apetrovic/cookiecutter/test/flask-aui-example/dev.db'>

The `urls` argument allows you to inspect all of the available routes the Flask server has::

    python manage.py urls
    Rule                                          Endpoint
    ---------------------------------------------------------------------------
    /                                             main.index
    /_debug_toolbar/static/<path:filename>        _debug_toolbar.static
    /_debug_toolbar/views/sqlalchemy/sql_explain  debugtoolbar.sql_select
    /_debug_toolbar/views/sqlalchemy/sql_select   debugtoolbar.sql_select
    /_debug_toolbar/views/template/<key>          debugtoolbar.template_editor
    /_debug_toolbar/views/template/<key>          debugtoolbar.template_preview
    /_debug_toolbar/views/template/<key>/save     debugtoolbar.save_template
    /static/<path:filename>                       static
    /static/<path:filename>                       main.static
    /users/                                       user.members
    /users/static/<path:filename>                 user.static

The `db` argument allows you to perform database migrations using the SQLAlchemy models defined in the project::

    python manage.py db
    Perform database migrations

    positional arguments:
      {upgrade,heads,show,migrate,stamp,current,merge,init,downgrade,branches,history,revision}
        upgrade             Upgrade to a later version
        heads               Show current available heads in the script directory
        show                Show the revision denoted by the given symbol.
        migrate             Alias for 'revision --autogenerate'
        stamp               'stamp' the revision table with the given revision;
                            don't run any migrations
        current             Display the current revision for each database.
        merge               Merge two revisions together. Creates a new migration
                            file
        init                Generates a new migration
        downgrade           Revert to a previous version
        branches            Show current branch points
        history             List changeset scripts in chronological order.
        revision            Create a new revision file.

See https://flask-migrate.readthedocs.org/en/latest/ for more information.


Running in Production
---------------------

The default approach taken in Development using Flask-Script and the manage.py entry-point does not scale well for production scale deployments. `gunicorn` 
is provided as a WSGI layer for running in production. This allows the application to run under multiple worker threads, which should handle the load adequately.

To run the server using `gunicorn`, run the following from the project's root directory::

    gunicorn -w 4 -b 0.0.0.0:8080 manage:app

This will spawn 4 worker threads, and expose the application globally via port 8080.
