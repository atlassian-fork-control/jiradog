from flask_assets import Bundle, Environment

css = Bundle(
    'libs/aui/css/aui.css',
    'libs/aui/css/aui-experimental.css',
    'css/style.css',
    filters='cssmin',
    output='public/css/common.css',
)


js = Bundle(
    'libs/aui/js/aui.js',
    'libs/aui/js/aui-experimental.js',
    'libs/aui/js/aui-soy.js',
    'js/plugins.js',
    'js/script.js',
    filters='jsmin',
    output='public/js/common.js',
)

assets = Environment()
assets.register("js_all", js)
assets.register("css_all", css)
