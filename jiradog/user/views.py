# -*- coding: utf-8 -*-
from flask import Blueprint, render_template

blueprint = Blueprint("user", __name__, url_prefix='/users',
                      static_folder="../static")


@blueprint.route("/")
def members():
        return render_template("users/members.html")
