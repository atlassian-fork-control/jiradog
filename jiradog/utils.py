# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""
from flask import flash
import json
import logging
import datetime
import tzlocal

LOCAL_TZ = tzlocal.get_localzone()


def flash_errors(form, category="warning"):
    """Flash all errors for a form."""
    for field, errors in form.errors.items():
        for error in errors:
            flash("{0} - {1}"
                  .format(getattr(form, field).label.text, error), category)


class JSONFormatter(logging.Formatter):
    """A formatter that renders log records as JSON objects.
    Format: {"timestamp":"...", "level":"...", "name":"...", "message":"..."}
    """
    def format(self, record):
        """Encode log record as JSON.
        """
        log = {
            "timestamp": self.formatTime(record, self.datefmt),
            "level": record.levelname,
            "name": record.name,
            "message": record.getMessage(),
        }
        if record.exc_info:
            log["traceback"] = self.formatException(record.exc_info)
        return json.dumps(log)
    def formatTime(self, record, datefmt=None):
        """Override default to use strftime, e.g. to get microseconds.
        """
        created = datetime.datetime.fromtimestamp(record.created, LOCAL_TZ)
        if datefmt:
            return created.strftime(datefmt)
        else:
            return created.strftime("%Y-%m-%dT%H:%M:%S.{:03d}%z".format(record.msecs))
